# DKX/Slim/NetteDI

Nette DI integration into Slim

## Installation

```bash
$ composer require dkx/slim-nette-di
```

## Usage

**config.neon:**

```yaml
extensions:
  slim: DKX\SlimNette\DI\SlimExtension
```

## Configuration

```php
<?php

use DKX\SlimNette\Configuration\ApplicationConfiguratorInterface;
use Slim\App;

final class AppConfiguration implements ApplicationConfiguratorInterface
{
    public function configure(App $app): void
    {
        $app->addRoutingMiddleware();
        // todo configure other middlewares and routes
    }
}
```

**config.neon:**

```yaml
extensions:
  slim: DKX\SlimNette\DI\SlimExtension

slim:
  configurators:
    - AppConfiguration

services:
  - AppConfiguration
```

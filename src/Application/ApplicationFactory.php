<?php

declare(strict_types=1);

namespace DKX\SlimNette\Application;

use DKX\SlimNette\Configuration\ApplicationConfiguratorInterface;
use Psr\Container\ContainerInterface;
use Slim\App;
use Slim\Factory\AppFactory;

final class ApplicationFactory
{
	/** @var \Psr\Container\ContainerInterface */
	private $container;

	/** @var \DKX\SlimNette\Configuration\ApplicationConfiguratorInterface[] */
	private $configurators = [];

	public function __construct(ContainerInterface $container)
	{
		$this->container = $container;
	}

	public function addConfigurator(ApplicationConfiguratorInterface $configurator): void
	{
		$this->configurators[] = $configurator;
	}

	public function createApplication(): App
	{
		$app = AppFactory::createFromContainer($this->container);

		foreach ($this->configurators as $configurator) {
			$configurator->configure($app);
		}

		return $app;
	}
}

<?php

declare(strict_types=1);

namespace DKX\SlimNette\Configuration;

use Slim\App;

interface ApplicationConfiguratorInterface
{
	public function configure(App $app): void;
}

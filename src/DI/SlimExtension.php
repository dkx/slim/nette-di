<?php

declare(strict_types=1);

namespace DKX\SlimNette\DI;

use DKX\NettePsr11\ContainerAdapter;
use DKX\SlimNette\Application\ApplicationFactory;
use Nette\DI\CompilerExtension;
use Nette\Schema\Expect;
use Nette\Schema\Schema;
use Psr\Http\Message\ResponseFactoryInterface;
use Slim\App;
use Slim\CallableResolver;
use Slim\Factory\AppFactory;

final class SlimExtension extends CompilerExtension
{
	public function getConfigSchema(): Schema
	{
		return Expect::structure([
			'configurators' => Expect::arrayOf('string'),
		]);
	}

	public function loadConfiguration()
	{
		/** @var mixed $config */
		$config = $this->config;
		$builder = $this->getContainerBuilder();

		$builder
			->addDefinition($this->prefix('psrContainer'))
			->setType(ContainerAdapter::class);

		$builder
			->addDefinition($this->prefix('callableResolver'))
			->setType(CallableResolver::class);

		$builder
			->addDefinition($this->prefix('responseFactory'))
			->setType(ResponseFactoryInterface::class)
			->setFactory([AppFactory::class, 'determineResponseFactory']);

		$factory = $builder
			->addDefinition($this->prefix('appFactory'))
			->setType(ApplicationFactory::class);

		foreach ($config->configurators as $configurator) {
			$factory->addSetup('addConfigurator', ['@'. $configurator]);
		}

		$builder
			->addDefinition($this->prefix('app'))
			->setType(App::class)
			->setFactory([$factory, 'createApplication']);
	}
}
